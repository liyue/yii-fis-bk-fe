$('select[name=package_id]').on('change',function(e){
    var $this = $(this);
    var package_id = $this.val();
    var $parent = $this.closest('tr');
    var $package_price = $parent.find('[name=package_price]');
    $.ajax({
        url: '/user-package/get-package-price',
        type:'get',
        data:{package_id:package_id},
        success:function(ret){
            $package_price.text(ret.data);
        }
    })
})
$('.action-btn-save').on('mousedown',function(e){
    var $this = $(this);
    var $parent = $this.closest('tr');
    var $package_price = $parent.find('[name=package_price]');
    var package_price = parseFloat($package_price.text());
    var $discount_price = $parent.find('input[name=discount_price]');
    var discount_price = parseFloat($discount_price.val()); 
    if(discount_price > package_price){
        alert('用户折扣价格必须低于原价,请重新输入折扣价格再保存！');
        e.preventDefault();
    }
})
