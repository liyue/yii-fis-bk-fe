<a class="action-btn action-btn-{%$action%} {%$class%}" ajax="{%$ajax%}" href="{%$href%}" target="{%$target%}"
action="{%$action%}" style="{%$style%}" cb_url="{%$cb_url%}" input_selector="{%$input_selector%}">{%$text%}</a>
{%script%}
    require('./add.js')
    require('./del.js')
    require('./save.js')
    require('./open.js')
    require('./ajax.js')
    require('./search.js')
    require('./select.js')
{%/script%}
