$('body').delegate('.btn-close','click',function(e){
    e.preventDefault();
    $close_target = $(this).closest('.close-target');
    if($close_target.length > 0){
        $close_target.hide();
    } else {
        $(this).parent().hide();
    }
})
