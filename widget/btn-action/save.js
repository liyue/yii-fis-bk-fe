var target_class = 'action-btn-save';
var target_selector = '.action-btn-save';
$(document).delegate(target_selector,'click',function(e){
    e.preventDefault();
    e.stopPropagation();
    var $that = $(this);
    var $form = $that.closest('form');
    var $tr = $that.closest('tr,li');
    if($form.length == 0){
        if($tr.length !=0){
            $form = $tr.find('form');
        } else {
            alert('保存失败;提交数据为空!');
            return;
        }
    }
    var $id = $tr.find('.id');
    var data =  new FormData($form[0]);
    $.ajax({
        url: $that.attr('ajax'),
        type: 'post',
        scriptCharset:'utf-8',
        processData: false,
        contentType: false,
        dataType: 'json',
        data: data,
        mimeType: "multipart/form-data",
        cache: false,
        success: function(data){
            if(data.code){
                var err_msg = '操作失败!';
                if(data.message){
                    err_msg +='失败原因:\n'+JSON.stringify(data.message);
                }
                else if(data.data.length>0){
                    err_msg +='失败原因:\n'+JSON.stringify(data.data);
                }
                alert(err_msg);
            }else{
                var extra_msg = '';
                if(data.message){
                    extra_msg = data.message
                }
                if(!$.isEmptyObject(data.data)){
                    $id.length&&$id.val(data.data);
                }
                alert('操作成功！'+extra_msg);
                // location.href = location.href;
            }
        },
        error: function (xhr, desc, err)
        {
            alert('操作失败,网络错误');
        }
    });
})
