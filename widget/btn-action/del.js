var $del_select = '.action-btn-del';
var $del_dom = $($del_select);
$('body').delegate($del_select,'mousedown',function(e){
    e.preventDefault();
    var r=confirm("亲,确认删除吗?不可逆哦!")
    if (r !=true)
    {
        return;
    }
    var $parent_dom = $(this).closest('tr,li');
    $.ajax({
        url: $(this).attr('ajax'),
        type: 'post',
        data: $parent_dom.data(),
        cache: false,
        success: function(ret){
            if(!ret)
            {
                alert('操作失败，请重试！');
            }
            else{
                $parent_dom.remove();
            }
        }
    });
})

