var $charge_select = '.action-btn-charge';
var $charge_dom = $($charge_select);
$(document).delegate($charge_select,'click',function(e){
    e.preventDefault();
    e.stopPropagation();
    var content=prompt("请输入金额","")
    if (content!=null && content!="")
    {
        if(isNaN(content)){
            alert("请输入正确的浮点数格式!");
            return;
        }
        var $parent_dom = $(this).closest('tr');
        var data = $parent_dom.data();
        data.trans_money = content;
        $.ajax({
            url: $(this).attr('ajax'),
            type: 'post',
            data: data,
            cache: false,
            success: function(data){
                if(data.code){
                    var err_msg = '操作失败!';
                    if(data.message){
                        err_msg +='失败原因:\n'+JSON.stringify(data.message);
                    }
                    else if(data.data.length>0){
                        err_msg +='失败原因:\n'+JSON.stringify(data.data);
                    }
                    alert(err_msg);
                }else{
                    var extra_msg = '';
                    if(data.message){
                        extra_msg = data.message
                    }
                    alert('操作成功！'+extra_msg);
                    if(data.data){
                        $parent_dom.find('td[name=balance]').html(data.data);
                    }
                    // location.href = location.href;
                }
            },
            error: function (xhr, desc, err)
            {
                alert('操作失败,网络错误');
            }
        });
    }
})

