<ul class="left-nav-list cf">
{%assign var=nav_list
    value= ['/'=>'仪表盘',
    '/data-api-supplier/index'=>'供应商管理',
    '/booking-api-log/index'=>'供应商对账',
    '/data-api/index'=>'api管理', 
    '/data-api-map/index'=>'api字段映射管理', 
    '/data-meta-item/index'=>'api标准字段管理', 
    '/data-package/index'=>'套餐管理', 
    '/data-package-api/item-index'=>'套餐=>数据项管理', 
    '/data-package-api/index'=>'套餐=>api管理', 
    '/user/index'=>'用户管理',
    '/admin/index'=>'管理员账户管理',
    '/user-package/index'=>'用户套餐折扣管理',
    '/booking/index'=>'订单管理',
    '/booking-item/index'=>'订单详细结果管理',
    '/trans-log/index'=>'交易记录',
    '/user/set-passwd'=>'密码修改'
]%}
{%assign var='arrUri' value=explode('?',$smarty.server.REQUEST_URI)%}
{%assign var='uri' value=$arrUri[0] scope="global"%}
{%if empty($class)%}{%assign var='class' value='red' scope="global"%}{%/if%}
{%if empty($nav)%}
    {%assign var=nav value=$nav_list[$uri] scope="global"%}
    {%if empty($nav)%}
        {%assign var=nav value="仪表盘" scope="global"%}
    {%/if%}
{%/if%}
    <input class="left-nav-search" name="left-nav" type="text" placeholder="按关键词搜索菜单"/> <hr/>
{%foreach $nav_list as $k=>$v%}
<a class="left-nav-item {%if $k == $uri%}{%$class%}{%/if%}"
href="{%$k%}" >{%$v%}</a>
{%/foreach%}
    <form enctype="multipart/form-data" action="/file/upload" method="post" class="left-nav-item">
        <button class="action-btn blue left-nav-upload-btn">上传图片</button>
        <input class="left-nav-upload" name="left-nav" type="file" class="left-nav-upload"/>
    </form>
{%script%}
    require('./left-nav.js')
{%/script%}
<!--<a class="left-nav-item" href="">产品管理</a>-->
<!--<a class="left-nav-item" href="">分类管理</a>-->
<!--<a class="left-nav-item" href="">用户管理</a>-->
<!--<a class="left-nav-item" href="">广告管理</a>-->
<!--<a class="left-nav-item" href="">公告管理</a>-->
<!--<a class="left-nav-item" href="">密码修改</a>-->
</ul>
