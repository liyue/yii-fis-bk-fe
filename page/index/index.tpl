{%extends file="manage/page/content-v2/content.tpl"%}
{%block name="right-content-head3"%}
{%require name="manage:static/common/underscore-min.js"%}
{%require name="manage:widget/btn-action/btn-action.css"%}
{%require name="manage:widget/btn-action/close.js"%}
{%/block%}
{%block name="right-content-table"%}
<ul class="content-instrument-list cf">
    <li class="content-instrument-item blue">
        <div class="content-item-num">{%$statistics.datapackage%}</div>
        <div class="content-item-label">套餐总数</div>
        <a class="content-item-link"
href="/data-package/index">查看详情</a>
    </li>
    <li class="content-instrument-item green">
        <div class="content-item-num">{%$statistics.booking%}</div>
        <div class="content-item-label">订单总数</div>
        <a class="content-item-link" href="/booking/index">查看详情</a>
    </li>
    <li class="content-instrument-item purple">
        <div class="content-item-num">{%$statistics.user%}</div>
        <div class="content-item-label">注册用户数</div>
        <a class="content-item-link" href="/user/index">查看详情</a>
    </li>
    <li class="content-instrument-item orange">
        <div class="content-item-num">{%$statistics.income%}</div>
        <div class="content-item-label">平台营业额</div>
        <a class="content-item-link" href="/trans-log/index">查看详情</a>
    </li>
</ul>
<div class="notice-wrap">
    <ul class="tab-list">
        <li class="tab-item cur">最新公告</li>
    </ul>
    <a class='action-btn blue action-btn-add' href="#">新增</a>
    <ul class="tab-content-list cf">
        {%foreach $system_notice.data as $notice%}
        <li data-id={%$notice.id%} class="aciton-parent tab-content-item cf">
        {%widget name="manage:widget/btn-action/btn-action.tpl" class="btn-close" action="del" text="X" ajax="/system-notice/delete"%}
        {%if !empty($notice.notice_image)%} 
        <img class="notice_img" src="{%$notice.notice_image%}" alt="图片暂无">
        {%else%} 
        <img class="notice_img" src="./notice-logo.png" alt="">
        {%/if%} 
        <label for="" class="notice_time">{%$notice.updated_at%}</label>
        <p class="notice_title">{%$notice.notice_title%}</p>
        <p class="notice_body">{%$notice.notice_body%}</p>
        </li>
        {%/foreach%}
    </ul>
    {%widget name="manage:widget/pagger/pagger.tpl" meta=$system_notice.meta%}
</div> 
<form action="" class="notice-form close-target">
    <input type="hidden" name="id" value="">
    <ul class="notice-list">
        {%widget name="manage:widget/btn-action/close.tpl"%}
        <li class="notice-item cf">
            <label for="input-notice-title">通知标题</label>
            <input type="text" name="notice_title" value="">
        </li>
        <li class="notice-item cf">
            <label for="input-notice-body">通知文字</label>
            <textarea type="text" name="notice_body"></textarea>
        </li>
        <li class="notice-item cf">
            <label for="input-notice-img">通知配图</label>
            <div class="img-wrap">
                <img src="./icon_upload_img.jpg" name="notice_img" class="input-img">
                <input class="img-input" name="notice_image" value="" type="file">
            </div> 
        </li>
        <li class="notice-item">
            {%widget name='manage:widget/btn-action/btn-action.tpl' text='确认' ajax='/system-notice/save' action='save' class='green' style='margin-left:50%;'%}
        </li>
    <ul>
</form> 
{%literal%}
<script type="text/template" id="tpl-notice-item">
    <li class="tab-content-item cf" data-mid="<%-data.mid%>">
        <label for=""><%=data.created_at%></label>
        <a class="btn-del" href="#">x</a>
        <p><%=data.content%></p>
    </li>
</script>
{%/literal%}
{%/block%}
