// 头部的时间控件
//上传图片即时显示
$('.right-filter-datetime').datetimepicker({
    format:'Y-m-d H:i',
    lang:'zh'
});
$(document).on('change','.img-input',function(e){
    var $fileInput = $(e.currentTarget);
    var $img = $(this).siblings('.input-img');
    var fileInput = $fileInput[0];
    if (fileInput.files && fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $img.attr('src',e.target.result);
        };
        reader.readAsDataURL(fileInput.files[0]);
    }
});
// sug
var $head = $('[sug]');
$head.each(function(index1,item1){
    var sug_index = $(item1).attr('index');
    $('tr').each(function(index,item){
        var sug_td = $(item).find('td').get(sug_index-1);
        var sug_source = $(item1).attr('sug');
        if(sug_td){
            $input = $(sug_td).children('input');
            // $(sug_td).on('keyup',sug_fun);
            $input.autocomplete({
                // source:availableTags
                source:sug_source,
                delay: 200
            })
        }
    })
});
$(document).on('mousemove', 'textarea', function(e) {
    var a = $(this).offset().top + $(this).outerHeight() - 16,  //  top border of bottom-right-corner-box area
    b = $(this).offset().left + $(this).outerWidth() - 16;  //  left border of bottom-right-corner-box area
    $(this).css({
        cursor: e.pageY > a && e.pageX > b ? 'nw-resize' : ''
    });
})
$('textarea').on('focus',function(e){
    $(this).css({
        'transform':'scale(2)',
        'z-index':999999
    });
})
$('textarea').on('blur',function(e){
    $(this).css({
        'transform':'',
        'z-index':0
    });
})
