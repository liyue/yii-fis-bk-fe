{%extends file="manage/page/base/base.tpl"%}
{%block name="right-main"%}
{%block name="assign-nav"%}
    {%require name="manage:static/common/jquery/plugin/jquery.datetimepicker.min.css"%}
    {%require name="manage:static/common/jquery/plugin/jquery.datetimepicker.full.min.js"%}
    {%require name="manage:static/common/jquery/plugin/jquery.removeAttributes.js"%}
{%/block%}
<div class="right-content-wrap cf">
    {%block name="right-content-head right-content-head1"%}
    <h3 class="right-content-head right-content-head1">{%if is_array($nav)%}{%$nav[0]%}{%else%}{%$nav%}{%/if%}</h3>
    {%/block%}
    {%block name="right-content-head2"%}
    <div class="right-content-head right-content-head2">管理后台 {%if is_string($nav)%}> {%$nav%}{%elseif is_array($nav)%}{%foreach $nav as $sub_nav%}>{%$sub_nav%}{%/foreach%} {%/if%}</div>
    {%/block%}
    {%block name="right-content-head3"%}
    <div class="right-content-head right-content-head3">
        {%foreach $head_filter as $name=>$value%}
            {%if !is_array($value) %}
                <input type="TEXT" class="right-content-search-input search-input" name="{%$name%}" value="{%$smarty.get[$name]%}" placeholder='{%$ar->getAttributeLabel($name)%}'>
                {%continue%}
            {%/if%}
            {%if  $value.type == text %}
                <input type="TEXT" class="right-content-search-input search-input" name="{%$name%}" value="{%$smarty.get[$name]%}" placeholder='{%$ar->getAttributeLabel($name)%}'>
            {%elseif $value.type == select %}
                <select id="" name="{%$name%}" class="action-btn action-btn-select">
                    {%foreach $value.option as $k => $v%}
                    <option value="{%$v%}" {%if !is_null($smarty.get[$name]) &&$v == $smarty.get[$name]%}selected=true{%/if%}>{%$k%}</option>
                    {%/foreach%}
                </select>
            {%elseif $value.type == datetime %}
                <input type="TEXT" class="right-filter-datetime right-content-search-input " name="{%$name%}" value="{%$smarty.get[$name]%}" placeholder='{%$ar->getAttributeLabel($name)%}'>
            {%/if%}
            {%if $value.line_break%}
            <div class="fl-split"></div>
            {%/if%}
        {%/foreach%}
        {%if !empty($head_filter)%}
            {%widget name='manage:widget/btn-action/btn-action.tpl' text='搜索' class='blue' action='search'%}
        {%/if%}
        {%if !empty($action[save])%}
            {%widget name='manage:widget/btn-action/btn-action.tpl' text='新增' class='green' action='add'%}
        {%/if%}
        {%if !empty($head_action)%}
            {%foreach $head_action as $head_action_1%}
            {%widget name='manage:widget/btn-action/btn-action.tpl' class='purple' text=$head_action_1.name href=$head_action_1.url action=$head_action_1.action %}
            {%/foreach%}
        {%/if%}
    </div>
    {%/block%}
    {%block name="right-content-table"%}
    <div class="right-table-wrap">
        <div class="right-table-header {%$class%}" for="">{%$nav%}</div>
        <table class="right-content-table" border=2>
            <thead>
                <tr>
                    {%assign var='index' value=1%}
                    {%foreach $head as $k=>$v%}
                        {%assign var="str_data" value=''%}
                        {%if !empty($v['extra'])%}
                            {%foreach $v['extra'] as $k1=>$extra %}
                                {%$str_data = "$str_data $k1=$extra"%}
                            {%/foreach%}
                        {%/if%}
                        {%if !empty($v['name'])%}
                            {%$v = $v['name']%}
                        {%else%}
                            {%$v = Yii::t('app',$ar->getAttributeLabel($k))%} 
                        {%/if%}
                        <th index={%$index%} {%$str_data%}>{%$v%}</th>
                        {%$index = $index+1%}
                    {%/foreach%}
                    {%$index = $index+1%}
                    <th index={%$index%}>操作</th>
                </tr>
            </thead>
            <tbody>
            {%for $i = 0;$i< 10;$i++%}
            {%assign var="oneData" value=[] %}
            {%foreach $head as $k=>$v%} 
                {%$oneData[$k] = null%}
            {%/foreach%}
            {%$data[] = $oneData%}
            {%/for%}
            {%foreach $data as $dataV%}
            <tr {%if !is_null($dataV[$id])%}data-{%$id%}= "{%$dataV[$id]%}"{%else%}class="tr-empty"{%/if%}>
                <form action="" class="t-form" enctype="multipart/form-data" >
                <input name="{%$id%}" class="id" type="hidden" value="{%$dataV[$id]%}"  >
                {%foreach $head as $headK=>$headV%}
                    {%assign var="name" value=$headK%}
                    {%assign var="value" value=$dataV[$name]%}
                    {%assign var="org_value" value=$value%}
                    {%if is_array($headV)%}
                        {%assign var="type" value=$headV.type%}
                        {%assign var="input_type" value=$headV.input_type%}
                        {%assign var="disabled" value=$headV.disabled%}
                        {%assign var="placeholder" value=$headV.placeholder%}
                        {%assign var="extra" value=$headV.extra%}
                        {%assign var="option" value=$headV.option%}
                        <!-- input类 -->
                        {%if empty($input_type)%}{%$input_type = 'text'%}{%/if%}
                        {%if $type == 'input'%}
                            {%$value = "<input name='$name' value='$value' type='$input_type' placeholder='$placeholder' {%if !empty($disabled)%}disabled{%/if%}>"%}
                            <!-- 子类image -->
                            {%if $input_type == 'image' %}
                                {%$value = "<img src='$org_value' class='input-img' />"%}
                                {%$value = "$value<input class='img-input' name='$name' value='$org_value' type='file'  {%if !empty($disabled)%}disabled{%/if%}/>"%}
                            <!-- 子类radio -->
                            {%elseif $input_type == 'radio'%}
                                {%$value = ""%}
                                {%foreach $option as $optionK => $optionV%}
                                    {%$input_value = ""%}
                                    {%if $dataV[$name] == $optionK%}
                                        {%$input_value ="<input name=$name value=$optionK checked=true type='radio' {%if !empty($disabled)%}disabled{%/if%}>"%}
                                    {%else%}
                                        {%$input_value ="<input name=$name value=$optionK type='radio' {%if !empty($disabled)%}disabled{%/if%}>"%}
                                    {%/if%}
                                    {%$value ="$value <div class='cf radio-wrap'>$input_value<label>$optionV</label></div>"%}
                                {%/foreach%}
                            <!-- 子类checkbox -->
                            {%elseif $input_type == 'checkbox'%}
                                {%$value = ""%}
                                {%foreach $option as $optionK => $optionV%}
                                    {%$input_value = ""%}
                                    {%if is_array($dataV[$name]) && in_array($optionK,$dataV[$name])%}
                                        {%$input_value ="<input name='$name[]' value=$optionK type='CHECKBOX' checked=true {%if !empty($disabled)%}disabled{%/if%}>"%}
                                    {%else%}
                                        {%$input_value ="<input name='$name[]' value=$optionK type='CHECKBOX' {%if !empty($disabled)%}disabled{%/if%}>"%}
                                    {%/if%}
                                    {%$value ="$value <div class='checkbox-wrap'>$input_value <span>$optionV</span></div>"%}
                                {%/foreach%}
                            {%/if%}
                        <!-- textarea类 -->
                        {%elseif $type == 'textarea'%}
                            {%$value = "<textarea name=$name  placeholder='$placeholder' {%if !empty($disabled)%}disabled{%/if%}>$value</textarea>"%}
                        <!-- select类 -->
                        {%elseif $type == 'select'%}
                            {%assign var="str_option" value=""%}
                            {%foreach $option as $optionK => $optionV%}
                                {%if $optionK == $value%}
                                    {%$str_option ="$str_option <option value=$optionK dataV=$optionV selected=true>$optionV</option>"%}
                                {%else%}
                                    {%$str_option ="$str_option <option value=$optionK dataV=$optionV >$optionV</option>"%}
                                {%/if%}
                            {%/foreach%}
                            {%$value = "<select name=$name {%if !empty($disabled)%}disabled{%/if%}>$str_option</select>"%}
                        {%elseif $type == 'a'%}
                            {%assign var="a_prefix" value=$headV.a_prefix%}
                            {%assign var="a_target" value=$headV.target%}
                            {%$value = "<a href=$a_prefix$value target='$a_target' >$value</a>"%}
                        {%elseif $type == 'img'%}
                            {%$value = "<img src=$value >"%}
                        {%/if%}
                    {%/if%}
                    <td name="{%$name%}" >{%$value|escape:none%}</td>
                {%/foreach%}

                <td class="btn-action-list">
                    <div class="btn-action-contain cf">
                    {%block name="btn-action-list"%}
                        {%assign var="colors" value=[red,green,blue,orange,purple]%}
                        {%assign var='i' value=0%}
                        {%foreach $action as $action_name=>$action_value%}
                        {%if is_null($action_value)%}
                            {%continue%}
                        {%/if%}
                        {%if is_array($action_value) %}
                            {%if !empty($action_value.action)%}
                                {%$action_name = $action_value.action%}
                            {%/if%}
                            {%if !empty($action_value.name)%}
                                {%assign var='trans_name' value=Yii::t('app',$action_value.name)%}
                            {%/if%}
                            {%if !empty($action_value.url)%}
                                {%assign var='action_url' value=Yii::t('app',$action_value.url)%}
                            {%/if%}
                        {%else%}
                            {%assign var='trans_name' value=Yii::t('app',$action_name)%}
                            {%assign var='action_url' value=Yii::t('app',$action_value)%}
                        {%/if%}
                            {%widget name='manage:widget/btn-action/btn-action.tpl' text=$trans_name href=$action_url ajax=$action_url action=$action_name class=$colors[$i]%}
                            {%$i = $i+1%}
                            {%require name = "manage:widget/btn-action/$action_name.js"%}
                            {%script%}
                                var js_path = "manage:widget/btn-action/{%$action_name%}.js";
                                require(js_path);
                            {%/script%}
                        {%/foreach%}
                        {%block name="btn-action-extra"%}
                        {%/block%}
                    {%/block%}
                    </div> 
                </td>
            </tr>
                </form> 
            {%/foreach%}
            </tbody>
        </table>
        {%widget name="manage:widget/pagger/pagger.tpl"%}
    </div> 
    {%/block%}
</div> 
{%block name="extra"%}
{%/block%}
{%/block%}
