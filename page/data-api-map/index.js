// 以下实现业务级别 select 和input 联动
$parent_ids = $('select[name=parent_item_id]');
$item_ids = $('select[name=item_id]');
$item_keys = $('input[name=item_key]');
$parent_ids.on('change',function(e){
    $tr = $(this).closest('tr');
    $item_key = $tr.find('input[name=item_key]');
    if($item_key.length >0){
        $item_key.val($(this).children('option:selected').text());
    }
});
$item_ids.on('change',function(e){
    $tr = $(this).closest('tr');
    $item_key = $tr.find('input[name=item_key]');
    if($item_key.length >0){
        $item_key.val($(this).children('option:selected').text());
    }
});
$item_keys.on('change',function(e){
    $tr = $(this).closest('tr');
    $item_id = $tr.find('select[name=item_id]');
    if($item_id.length >0){
        var text = $(this).val();
        // var target_option = $item_id.find(`option:contains("'${text}'")`);
        var target_option = $item_id.find(`option[dataV='${text}']`);
        if(target_option.length == 1){
            target_option.prop('selected',true);
        }
    }
})
$item_keys.on("autocompleteselect",function(e,ui){
    var text = ui.item.value;
    $tr = $(this).closest('tr');
    $item_id = $tr.find('select[name=item_id]');
    if($item_id.length >0){
        // var target_option = $item_id.find(`option:contains("'${text}'")`);
        var target_option = $item_id.find(`option[dataV='${text}']`);
        if(target_option.length == 1){
            target_option.prop('selected',true);
        }
    }

})
